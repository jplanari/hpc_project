
int quisoc()
{
	int q;
	MPI_Comm_rank(MPI_COMM_WORLD,&q);
	return(q);
}

int quants()
{
	int q;
	MPI_Comm_size(MPI_COMM_WORLD,&q);
	return(q);
}

void worksplit(int *mystart,int *myend, int proc, int nproc, int start,int end)
{
	  int info=0;
	  int ntasks,res,mytasks;
	  ntasks=end-start+1; // total number of tasks
	  res=ntasks%nproc;
	  if (nproc<1 || end<start || proc<0 || proc>nproc-1) {
	    printf("Uhhh wrong parameters nproc=%d start=%d end=%d \n",nproc, start, end);
	    exit(-1);
	  }
	  int nHard = ntasks / nproc + 1; // procs from 0 to res-1 do hard work
	  int nLazy = ntasks / nproc; // the rest, lazy work
	  int lastHardTask = nHard * res + start;

	  if (info>1) printf("res=%d nHard=%d nLazy=%d lastHardTask=%d \n",
	    res,nHard,nLazy,lastHardTask);

	  if (proc<=res-1) { // Hard
	    *mystart=  proc*nHard+start;
	    *myend  = *mystart+nHard-1;
	  } else { // Lazy
	    *mystart= (proc-res)*nLazy +lastHardTask; // proc doing L * L + all the hard
	    *myend = *mystart+ nLazy-1;
	  }
}
