#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "mpi.h"
#include "Basic_Functions.h"

struct map
{
  int MYS_Y, MYE_Y; //j-coordinate of the start and end of the processor domain
  int MYS_X, MYE_X; //i-coordinate of the start of the processor domain
  int NEIGH_NORTH, NEIGH_SOUTH; //define the neighbour processors

  int DOMAIN_SIZE;

  int HN_START_X, HN_END_X, HN_START_Y, HN_END_Y;
  int HS_START_X, HS_END_X, HS_START_Y, HS_END_Y;

  int H_NEIGH_N_START_X, H_NEIGH_N_END_X, H_NEIGH_N_START_Y, H_NEIGH_N_END_Y;
  int H_NEIGH_S_START_X, H_NEIGH_S_END_X, H_NEIGH_S_START_Y, H_NEIGH_S_END_Y;
};

#define ForY(i,M) for(i = (M->MYS_Y); i <= (M->MYE_Y); i++)
#define ForX(i,M) for(i = (M->MYS_X); i <= (M->MYE_X); i++)
#define ForAll(i,j,M) ForY(j,M) ForX(i,M)

#define DomainSizeX(M) ((M->MYE_X) - (M->MYS_X) + 1)
#define DomainSizeY(M) ((M->MYE_Y) - (M->MYS_Y) + 1)
#define DomainSize(M) ((DomainSizeX(M))*(DomainSizeY(M)))

#define ForHaloX_N(i,M) for(i = (M->HN_START_X); i <= (M->HN_END_X); i++)
#define ForHaloY_N(i,M) for(i = (M->HN_START_Y); i <= (M->HN_END_Y); i++)

#define ForAllHalo_N(i,j,M) ForHaloY_N(j,M) ForHaloX_N(i,M)

#define ForHaloX_S(i,M) for(i = (M->HS_START_X); i <= (M->HS_END_X); i++)
#define ForHaloY_S(i,M) for(i = (M->HS_START_Y); i <= (M->HS_END_Y); i++)

#define ForAllHalo_S(i,j,M) ForHaloY_S(j,M) ForHaloX_S(i,M)

#define ForHaloX_N_NEIGH(i,M) for(i = (M->H_NEIGH_N_START_X); i <= (M->H_NEIGH_N_END_X); i++)
#define ForHaloY_N_NEIGH(i,M) for(i = (M->H_NEIGH_N_START_Y); i <= (M->H_NEIGH_N_END_Y); i++)

#define ForAllHalo_N_NEIGH(i,j,M) ForHaloY_N_NEIGH(j,M) ForHaloX_N_NEIGH(i,M)

#define ForHaloX_S_NEIGH(i,M) for(i = (M->H_NEIGH_S_START_X); i <= (M->H_NEIGH_S_END_X); i++)
#define ForHaloY_S_NEIGH(i,M) for(i = (M->H_NEIGH_S_START_Y); i <= (M->H_NEIGH_S_END_Y); i++)

#define ForAllHalo_S_NEIGH(i,j,M) ForHaloY_S_NEIGH(j,M) ForHaloX_S_NEIGH(i,M)

#define A(i,j,a,M) *(a + i - M->MYS_X + (j-M->HS_START_Y)*DomainSizeX(M))
#define B(i,j,a,M) *(a + i - M->MYS_X + (j-M->HS_START_Y)*DomainSizeX(M))
#define C(i,j,a,M) *(a + i - M->MYS_X + (j-M->HS_START_Y)*DomainSizeX(M))

#define X(i,j,a,M) *(a + i - M->MYS_X + (j-M->HS_START_Y)*DomainSizeX(M))
#define Y(i,j,a,M) *(a + i - M->MYS_X + (j-M->HS_START_Y)*DomainSizeX(M))

#define AP(i,j,A,M) *(A + (i - M->MYS_X + (j-M->HS_START_Y)*DomainSizeX(M))*5)
#define AE(i,j,A,M) *(A + (i - M->MYS_X + (j-M->HS_START_Y)*DomainSizeX(M))*5+1)
#define AW(i,j,A,M) *(A + (i - M->MYS_X + (j-M->HS_START_Y)*DomainSizeX(M))*5+2)
#define AN(i,j,A,M) *(A + (i - M->MYS_X + (j-M->HS_START_Y)*DomainSizeX(M))*5+3)
#define AS(i,j,A,M) *(A + (i - M->MYS_X + (j-M->HS_START_Y)*DomainSizeX(M))*5+4)


void fulfillMap(struct map *M, int Nx, int Ny, int stencil)
{
  int mys, mye;
  worksplit(&mys,&mye,quisoc(),quants(),1,Ny);

  M->MYS_Y = mys;
  M->MYE_Y = mye;
  M->MYS_X = 1;
  M->MYE_X = Nx;

  M->HN_START_X = M->MYS_X;
  M->HS_START_X = M->MYS_X;
  M->HN_END_X = M->MYE_X;
  M->HS_END_X = M->MYE_X;

  M->HN_START_Y = mye+1;
  M->HN_END_Y = mye + stencil;

  M->HS_START_Y = mys-stencil;
  M->HS_END_Y = mys-1;

  M->H_NEIGH_N_START_X = M->MYS_X;
  M->H_NEIGH_N_END_X = M->MYE_X;
  M->H_NEIGH_N_START_Y = M->MYE_Y-stencil+1;
  M->H_NEIGH_N_END_Y = M->MYE_Y;

  M->H_NEIGH_S_START_X = M->MYS_X;
  M->H_NEIGH_S_END_X = M->MYE_X;
  M->H_NEIGH_S_START_Y = M->MYS_Y;
  M->H_NEIGH_S_END_Y = M->MYS_Y + stencil - 1;

  if(quisoc() == 0)
  {
    M->NEIGH_NORTH = quisoc() + 1;
    M->NEIGH_SOUTH = quants() - 1;
  }
  else if(quisoc() == quants() - 1)
  {
    M->NEIGH_NORTH = 0;
    M->NEIGH_SOUTH = quisoc() - 1;
  }
  else
  {
    M->NEIGH_NORTH = quisoc() + 1;
    M->NEIGH_SOUTH = quisoc() - 1;
  }

  M->DOMAIN_SIZE = (M->MYE_Y-M->MYS_Y+1)*(M->MYE_X-M->MYS_X+1);

}

void printMap(struct map *M)
{
  printf("proc = %d/%d, mys_x = %d, mye_x = %d, mys_y = %d, mye_y = %d\n",quisoc(),quants()-1,M->MYS_X,M->MYE_X,M->MYS_Y,M->MYE_Y);

  printf("proc = %d/%d, neigh_north = %d, halos_x = %d, haloe_x = %d, halos_y = %d, haloe_y = %d\n",quisoc(),quants()-1,M->NEIGH_NORTH,M->HN_START_X,M->HN_END_X,M->HN_START_Y,M->HN_END_Y);
  printf("proc = %d/%d, neigh_south = %d, halos_x = %d, haloe_x = %d, halos_y = %d, haloe_y = %d\n",quisoc(),quants()-1,M->NEIGH_SOUTH,M->HS_START_X,M->HS_END_X,M->HS_START_Y,M->HS_END_Y);

  printf("proc = %d/%d, neigh_north = %d, hishalos_x = %d, hishaloe_x = %d, hishalos_y = %d, hishaloe_y = %d\n",quisoc(),quants()-1,M->NEIGH_NORTH,M->H_NEIGH_N_START_X,M->H_NEIGH_N_END_X,M->H_NEIGH_N_START_Y,M->H_NEIGH_N_END_Y);
  printf("proc = %d/%d, neigh_south = %d, hishalos_x = %d, hishaloe_x = %d, hishalos_y = %d, hishaloe_y = %d\n",quisoc(),quants()-1,M->NEIGH_SOUTH,M->H_NEIGH_S_START_X,M->H_NEIGH_S_END_X,M->H_NEIGH_S_START_Y,M->H_NEIGH_S_END_Y);

  printf("\n");
}

void printValues(double *a, struct map *M, int Nx)
{
  for(int j = M->MYS_Y; j <= M->MYE_Y; j++)
  {
    for(int i = M->MYS_X; i <= M->MYE_X; i++)
    {
      printf("proc = %d, i = %d, j = %d, a[i][j] = %2.1f\n",quisoc(),i,j,*(a+(i-M->MYS_X)+(j-M->HS_START_Y)*Nx));
    }
  }
  printf("\n");
}

void printPack(double *v, int Nx)
{
  for(int i = 0; i <= Nx-1; i++)
  {
    printf("proc = %d, i=%d, v[i] = %0.2f\n",quisoc(),i,*(v+i));
  }
}

void pack(double *v, double *a, int opt, int Nx, struct map *M)
{
  int i,j;
  int halosize = Nx;
  int domainsize_x = Nx;
  if(opt == 0) //Pack halo at the north of the domain to send to neighbour at north
  {
    ForAllHalo_N_NEIGH(i,j,M)
    {
      *(v+(i-M->H_NEIGH_N_START_X)+(j-M->H_NEIGH_N_START_Y)*halosize) = A(i,j,a,M);
    }
  }
  else if(opt == 1) //Pack halo at the south of the domain to send to neighbour at south
  {
    ForAllHalo_S_NEIGH(i,j,M)
    {
      *(v+(i-M->H_NEIGH_S_START_X)+(j-M->H_NEIGH_S_START_Y)*halosize) = A(i,j,a,M);
    }
  }
  else
  {
    printf("Error in the option chosen\n");
    exit(-1);
  }
}

void unpack(double *v, double *a, int opt, int Nx, struct map *M)
{
  int i,j;
  int halosize = Nx;
  int domainsize_x = Nx;

  if(opt == 0) //Place on its position the halo received from the north neighbour
  {
    ForAllHalo_N(i,j,M)
    {
      A(i,j,a,M) = *(v+(i-M->HN_START_X)+(j-M->HN_START_Y)*halosize);
    }
  }
  else if(opt == 1) //Place on its position the halo received from the south neighbour
  {
    ForAllHalo_S(i,j,M)
    {
      A(i,j,a,M) = *(v+(i-M->HS_START_X)+(j-M->HS_START_Y)*halosize);
    }
  }
  else
  {
    printf("Error in the option chosen\n");
    exit(-1);
  }
}

void haloUpdate(struct map *M, double* a, int Nx, int stencil)
{
  int i,j;

  int halosize = Nx*stencil;

  double *v;
  v = (double*)malloc(sizeof(double)*Nx*stencil);

  if(quisoc()%2 == 0)
  {
    pack(v,a,0,Nx,M);
    MPI_Ssend(v,halosize,MPI_DOUBLE,M->NEIGH_NORTH,0,MPI_COMM_WORLD);
  }
  else
  {
    MPI_Recv(v,halosize,MPI_DOUBLE,M->NEIGH_SOUTH,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
    unpack(v,a,1,Nx,M);
  }

  MPI_Barrier(MPI_COMM_WORLD);

  if(quisoc()%2 == 0)
  {
    MPI_Recv(v,halosize,MPI_DOUBLE,M->NEIGH_SOUTH,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
    unpack(v,a,1,Nx,M);
  }
  else
  {
    pack(v,a,0,Nx,M);
    MPI_Ssend(v,halosize,MPI_DOUBLE,M->NEIGH_NORTH,0,MPI_COMM_WORLD);
  }

  MPI_Barrier(MPI_COMM_WORLD);

  if(quisoc()%2 == 0)
  {
    MPI_Recv(v,halosize,MPI_DOUBLE,M->NEIGH_NORTH,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
    unpack(v,a,0,Nx,M);
  }
  else
  {
    pack(v,a,1,Nx,M);
    MPI_Ssend(v,halosize,MPI_DOUBLE,M->NEIGH_SOUTH,0,MPI_COMM_WORLD);
  }

  MPI_Barrier(MPI_COMM_WORLD);

  if(quisoc()%2 == 0)
  {
    pack(v,a,1,Nx,M);
    MPI_Ssend(v,halosize,MPI_DOUBLE,M->NEIGH_SOUTH,0,MPI_COMM_WORLD);
  }
  else
  {
    MPI_Recv(v,halosize,MPI_DOUBLE,M->NEIGH_NORTH,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
    unpack(v,a,0,Nx,M);
  }

  free(v);

}

void fulfillVectors(struct map *M, double* a, double value)
{
  int i,j;
  ForAll(i,j,M)
    A(i,j,a,M) = value;
}

void setVector(double* a)
{

}

double vectorVector (struct map *M, double *a, double *b) //returns s = a'*b
{
  int i,j;
  double s=0, sg;
  ForAll(i,j,M) s += A(i,j,a,M)*B(i,j,b,M);
  MPI_Allreduce(&s,&sg,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  return sg;
}

void linearCombination (struct map *M, double *c, double x, double *a, double y, double *b) // returns c = x*a+y*b;
{
  int i,j;
  ForAll(i,j,M)
    C(i,j,c,M) = x*A(i,j,a,M) + y*B(i,j,b,M);
}

void matrixVector (struct map *M, double *y, double *A, double *x, int Ny) //returns y = A*x, assumes halo is updated
{
  int i,j;
  double xe=0, xw=0, xn=0, xs=0;
  int info = 0;
  ForAll(i,j,M)
  {
    if(DomainSizeX(M)==1)
    {
      xe=0.0;
      xw = 0.0;
    }
    else
    {
      if(i==1) {
        xw = 0.0;
        xe = X(i+1,j,x,M);
      } else if(i == DomainSizeX(M)) {
        xe = 0.0;
        xw = X(i-1,j,x,M);
      } else {
        xe = X(i+1,j,x,M);
        xw = X(i-1,j,x,M);
      }
    }

    if(j == 1){
      xs = 0.0;
      xn = X(i,j+1,x,M);
    } else if(j == Ny) {
      xn = 0.0;
      xs = X(i,j-1,x,M);
    } else {
      xn = X(i,j+1,x,M);
      xs = X(i,j-1,x,M);
    }

    if(info) printf("i=%d, j=%d, xn=%2.0f, xs=%2.0f, xe=%2.0f, xw=%2.0f\n",i,j,xn,xs,xe,xw);

    Y(i,j,y,M) = AP(i,j,A,M)*X(i,j,x,M) - AE(i,j,A,M)*xe - AW(i,j,A,M)*xw - AN(i,j,A,M)*xn - AS(i,j,A,M)*xs;


  }
}

void fulfillCoefficients(struct map *M, double *A, int Ny)
{
  int i,j;
  ForAll(i,j,M)
  {
    if(j == Ny)
      AP(i,j,A,M) = 1.0;
    else
      AP(i,j,A,M) = 2.0;

    if(j == Ny)
      AN(i,j,A,M) = 0.0;
    else
      AN(i,j,A,M) = 1.0;

    if(j == 1)
      AS(i,j,A,M) = 0.0;
    else
      AS(i,j,A,M) = 1.0;

    AE(i,j,A,M) = 0.0;
    AW(i,j,A,M) = 0.0;
  }
}

void printCoefficients(struct map *M, double *A)
{
  int i,j;
  ForAll(i,j,M)
  {
    printf("i=%d, j=%d, AE = %1.1f, AW = %1.1f, AN = %1.1f, AS = %1.1f, AP = %1.1f\n",i,j,AE(i,j,A,M),AW(i,j,A,M),AN(i,j,A,M),AS(i,j,A,M),AP(i,j,A,M));
  }
}


void conjugateGradientMethod(struct map *M, double *A, double *x, double *b, double tolerance, int Ny)
{
  double* Ax = (double*)malloc(sizeof(double)*(M->DOMAIN_SIZE+2*DomainSizeX(M)));
  double* r = (double*)malloc(sizeof(double)*(M->DOMAIN_SIZE+2*DomainSizeX(M)));
  double* p = (double*)malloc(sizeof(double)*(M->DOMAIN_SIZE+2*DomainSizeX(M)));
  double alpha, beta, r_sq;
  int info = 0;
  double stencil = 1;

  haloUpdate(M,x,DomainSizeX(M),stencil);
  matrixVector(M,Ax,A,x,Ny);
  if(info) {printf("Ax=\n");
  printValues(Ax,M,DomainSizeX(M));
  getchar();
  MPI_Barrier(MPI_COMM_WORLD);}
  linearCombination(M,r,1,b,-1,Ax);
  if(info){
    printf("r=\n");
    printValues(r,M,DomainSizeX(M));
    getchar();
    MPI_Barrier(MPI_COMM_WORLD);
  }
  linearCombination(M,p,1,r,0,r);
  if(info){
    printf("p=\n");
    printValues(p,M,DomainSizeX(M));
    getchar();
    MPI_Barrier(MPI_COMM_WORLD);
  }
  while(sqrt(vectorVector(M,r,r))>tolerance)
  {
    r_sq = vectorVector(M,r,r);
    if(info){printf("rsq=%e\n",r_sq);}
    haloUpdate(M,p,DomainSizeX(M),stencil);
    matrixVector(M,Ax,A,p,Ny);
    if(info){
      printf("Ap=\n");
      printValues(Ax,M,DomainSizeX(M));
      getchar();
      MPI_Barrier(MPI_COMM_WORLD);
    }
    alpha = r_sq/vectorVector(M,p,Ax);
    if(info){printf("alpha=%e\n",alpha);}
    linearCombination(M,x,1,x,alpha,p);
    if(info){
      printf("x=\n");
      printValues(x,M,DomainSizeX(M));
      getchar();
      MPI_Barrier(MPI_COMM_WORLD);
    }
    linearCombination(M,r,1,r,-alpha,Ax);
    if(info){
      printf("r=\n");
      printValues(r,M,DomainSizeX(M));
      getchar();
      MPI_Barrier(MPI_COMM_WORLD);
    }
    if(sqrt(vectorVector(M,r,r))<tolerance)
    {
      break;
    }
    else
    {
      beta = vectorVector(M,r,r)/r_sq;
      if(info) printf("beta=%e\n",beta);
      linearCombination(M,p,1,r,beta,p);
      if(info){
        printf("p=\n");
        printValues(p,M,DomainSizeX(M));
        getchar();
        MPI_Barrier(MPI_COMM_WORLD);
      }
    }
  }

  free(Ax);
  free(r);
  free(p);

}

void validateMethod(struct map *M, double *A, double *x, double *b, double tolerance, int Ny)
{
  double* Ax = (double*)malloc(sizeof(double)*(M->DOMAIN_SIZE+2*DomainSizeX(M)));
  double* r = (double*)malloc(sizeof(double)*(M->DOMAIN_SIZE+2*DomainSizeX(M)));

  matrixVector(M,Ax,A,x,Ny);
  linearCombination(M,r,1,b,-1,Ax);

  if(sqrt(vectorVector(M,r,r))>tolerance)
  {
    printf("Not solved properly, rsq = %e\n",sqrt(vectorVector(M,r,r)));
  }
  else
  {
    printf("Solved properly, rsq = %e\n",sqrt(vectorVector(M,r,r)));
  }

  free(Ax);
  free(r);
}


int main(int argc, char** argv)
{
  double* a;
  double* b;
  double* c;
  struct map M;
  int stencil = 1;
  int info = 0;

  MPI_Init(&argc,&argv);

  if(argc != 3) {printf("mpirun -np N ./a.out Nx Ny"); return(-1);}

  int Nx = atoi(argv[1]);
  int Ny = atoi(argv[2]); //

  fulfillMap(&M, Nx, Ny, stencil);

  if (info) printMap(&M);

  a = (double*)malloc(sizeof(double)*(M.DOMAIN_SIZE+2*Nx));
  b = (double*)malloc(sizeof(double)*(M.DOMAIN_SIZE+2*Nx));
  c = (double*)malloc(sizeof(double)*(M.DOMAIN_SIZE+2*Nx));

  fulfillVectors(&M,a,2.0);
  fulfillVectors(&M,b,1.0);

  MPI_Barrier(MPI_COMM_WORLD);


  double *A = (double*)malloc(sizeof(double)*(M.DOMAIN_SIZE+2*Nx)*5);

  //printf("Conjugate Gradient test\n");
  fulfillCoefficients(&M,A,Ny);
  if(info) printCoefficients(&M,A);


  fulfillVectors(&M,b,1.0);

  double tol = 1e-5;

  conjugateGradientMethod(&M,A,a,b,tol,Ny);
  //validateMethod(&M,A,a,b,tol);



  //matrixVector(&M,b,A,a,Ny);

  printValues(a,&M,Nx);

  free(a);
  free(b);
  free(c);
  free(A);

  MPI_Finalize();

  exit(0);
}
